Package API
===========

Wavefront
----------

.. automodule:: pyoptica
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:

Optical Elements
----------------

.. automodule:: pyoptica.optical_elements
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:

Imaging System
-------

.. automodule:: pyoptica.imaging_system
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:

Zernikes
--------

.. automodule:: pyoptica.zernikes
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:

Utils
-----

.. automodule:: pyoptica.utils
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:

Holography
----------

.. automodule:: pyoptica.holography
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:

Plotting
--------

.. automodule:: pyoptica.plotting
    :exclude-members: __dict__, __weakref__
    :members:
    :undoc-members:
    :show-inheritance:
