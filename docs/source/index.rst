.. pyoptica documentation master file, created by
   sphinx-quickstart on Thu Jun  6 19:13:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyoptica's documentation!
====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   api.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
