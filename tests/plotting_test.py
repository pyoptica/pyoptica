import unittest

import astropy.units as u
import numpy as np

import pyoptica as po


class PlotableExample(po.plotting.Plottable):
    PLOTTING_OPTIONS = dict(
        first=dict(x='x', y='y', title=''), second=dict(x='x', y='y', title='')
    )

    @property
    def first(self):
        return np.ones((10, 10))

    @property
    def second(self):
        return np.ones((10, 10))

    @property
    def x(self):
        return np.meshgrid(np.arange(10), np.arange(10))[0] * u.m

    @property
    def y(self):
        return np.meshgrid(np.arange(10), np.arange(10))[1] * u.m


class TestPlottable(unittest.TestCase):
    def test_plottables(self):
        test_object = PlotableExample()
        self.assertListEqual(["first", 'second'], test_object.plottables)

    def test_plot_check_input_raises(self):
        test_object = PlotableExample()
        with self.assertRaises(ValueError):
            _ = test_object.plot(third=dict())

    def test_plot_check_input_doesnt_raises(self):
        test_object = PlotableExample()
        _ = test_object.plot(first='default')

    def test_plot_check_options_raises(self):
        test_object = PlotableExample()
        with self.assertRaises(ValueError):
            _ = test_object.plot(first=dict(aaa=1))

    def test_plot_check_input_doesnt_raises(self):
        test_object = PlotableExample()
        _ = test_object.plot(first=dict(title="aaaa"))

    def test_plot_check_options_doesnt_raise_on_default(self):
        test_object = PlotableExample()
        _ = test_object.plot(first='default')


if __name__ == '__main__':
    unittest.main()
