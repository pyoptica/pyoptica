import unittest

import astropy.units as u
import numpy as np

from pyoptica.optical_elements.siemens_star import SiemensStar
from pyoptica.wavefront import Wavefront


class TestSiemensStar(unittest.TestCase):
    def test_init(self):
        cycles = 10
        star = SiemensStar(cycles)
        self.assertEqual(star.cycles, cycles)

    def test_amplitude_transmittance(self):
        wavelength = 1000 * u.nm
        npix = 512
        pix_scale = 100 * u.um
        wf = Wavefront(wavelength, pix_scale, npix)
        cycles = 2
        star = SiemensStar(cycles)
        amp = (wf * star).amplitude
        phs = (wf * star).phase
        test_set = [(amp[npix // 2 + 100, npix // 2 + 100], 1),
                    (amp[npix // 2 + 100, npix // 2 - 100], 0),
                    (amp[npix // 2 - 100, npix // 2 + 100], 0),
                    (amp[npix // 2 - 100, npix // 2 - 100], 1),
                    (np.mean(phs), 0)
                    ]
        for selected, expected in test_set:
            with self.subTest(selected=selected, expected=expected):
                self.assertEqual(selected, expected)

    def test_phase_transmittance(self):
        wavelength = 1000 * u.nm
        npix = 512
        pix_scale = 100 * u.um
        wf = Wavefront(wavelength, pix_scale, npix)
        cycles = 2
        kind = 'phase'
        star = SiemensStar(cycles, kind)
        amp = (wf * star).amplitude
        phs = (wf * star).phase
        test_set = [(phs[npix // 2 + 100, npix // 2 + 100], np.pi),
                    (phs[npix // 2 + 100, npix // 2 - 100], 0),
                    (phs[npix // 2 - 100, npix // 2 + 100], 0),
                    (phs[npix // 2 - 100, npix // 2 - 100], np.pi),
                    (np.mean(amp), 1)
                    ]
        for selected, expected in test_set:
            with self.subTest(selected=selected, expected=expected):
                self.assertEqual(selected, expected)
