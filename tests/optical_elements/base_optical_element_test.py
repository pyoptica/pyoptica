import unittest

from pyoptica import BaseOpticalElement


class TestBaseOpticalElement(unittest.TestCase):
    def test_is_abstract(self):
        with self.assertRaises(TypeError):
            BaseOpticalElement()
