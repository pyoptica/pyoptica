{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"../pyoptica/data/logos/pyoptica_round.png\" alt=\"PyOptica Logo\" width=\"100\"/>\n",
    "\n",
    "# Welcome to PyOptica! \n",
    "\n",
    "PyOptica is a package for simulation of wave optics in Python. It is developed to deal with optics simulations in a *pythonic* way; it is one of the most important presupposition of the whole project to follow the Zen of Python and create a structure that is known to users from the most popular scientific packages: NumPy or SciPy.\n",
    "\n",
    "## The Goal\n",
    "\n",
    "The package is meant to provide functionality to propagate a wavefront through space, interact with optical elements (e.g. lenses or apertures), build optical systems, and to implement basic algorithms for holography. As I am writing these words not everything listed above has already been developed, however, we are doing are best to develop everything. Of course we would also like to invite **YOU** to the development of the package. Everybody is welcome –  both experienced developers or optical engineers and students who have just embarked on their adventure with optics. \n",
    "\n",
    "Not only are we developing a software package but also we are committed to work on a series of Notebooks that would solve problems described in the most popular books on Optics. There is also a room for potential expansion of what is developed: you use the package to verify that your solution of a given problem is correct? Make a notebook out of it!\n",
    "\n",
    "## About Us\n",
    "\n",
    "Our names are Maciej Grochowicz and Michał Miler. I guess our names may sound unfamiliar to you – we are from Poland where we also studied Applied Physics at Warsaw University of Technology. Right now we are professional engineers. In case you would like to learn more about us, please visit our Linkedin profiles:\n",
    "\n",
    "* Maciej Grochowicz: https://www.linkedin.com/in/maciej-grochowicz-b70a72b4/\n",
    "* Michał Miler: https://www.linkedin.com/in/milerm/\n",
    "\n",
    "## Literature\n",
    "Although we would love to say that we relied solely on our knowledge and experience, we are obliged to list all of the books we used during the development: \n",
    "1. Joseph W. Goodman (2004) *Introduction to Fourier Optics*, W. H. Freeman\n",
    "2. Kedar Khare (2016) *Fourier Optics and Computational Imaging*, Wiley&Sons Ltd.\n",
    "3. David Voelz (2011) *Computational Fourier Optics Matlab Tutorial*, Spie Press"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Let's get started!\n",
    "\n",
    "Let's start by exploring the basic functionality of the package that is propagation of light and interaction with a thin lens. Before we do that let's look at the imports."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import astropy.units as u\n",
    "import numpy as np\n",
    "\n",
    "import pyoptica as po"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We decided to use `astropy.units` to track physical units used in the simulations; it can be done in a very elegant, unambiguous way. Let's see by defining couple of constants required to create an instance of `Wavefront` that is: \n",
    "* `wavelength` – that is self-explanatory; \n",
    "* `pixel_scale` – size of one pixel, sampling of the wavefront;\n",
    "* `npix` – number of pixels in the wavefront. \n",
    "By multiplying `npix` and `pixel_scale` you can find the size of your wavefront. \n",
    "\n",
    "We can see how the created wavefront looks like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavelength = 500 * u.nm # you see how easy it is? Just multiply your value by the unit!\n",
    "pixel_scale = 0.022 * u.mm\n",
    "npix = 1024\n",
    "\n",
    "w = 6 * u.mm\n",
    "h = 3 * u.mm\n",
    "axis_unit = u.mm\n",
    "\n",
    "wf = po.Wavefront(wavelength, pixel_scale, npix)\n",
    "ap = po.RectangularAperture(w, h)\n",
    "wf = wf * ap\n",
    "_ = wf.plot(\n",
    "    intensity='default',\n",
    "    amplitude='default',\n",
    "    phase='default', \n",
    "    fig_options=dict(figsize=(10, 10), dpi=130)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The subsequent step is to propagate the wavefront. How are we going to do that? By using a method to propagate using *Angular Spectrum of Plane Waves*. It is relatively straightforward to compute; following Fourier Optics by J. Goodman (Chapter 5.1):\n",
    "\n",
    "$$U_{2}(x_2, y_2)=\\mathfrak{F}^{-1}\\left\\{\\mathfrak{F}\\left\\{U_{1}(x_1, y_1)\\right\\} H\\left(f_{X}, f_{Y}\\right)\\right\\}$$\n",
    "where:\n",
    "$U_{1}(x, y)$ – the initial wavefront,\n",
    "$H\\left(f_{X}, f_{Y}\\right)=\\exp\\left(j k z\\right) \\exp \\left(-j \\pi\\lambda z\\left(f_{X}^{2}+f_{Y}^{2}\\right)\\right)$ – the transfer fuction.\n",
    "\n",
    "It is computed using the following convolution theorem:\n",
    "$$f \\ast g =\\mathfrak{F}^{-1}\\left\\{\\mathfrak{F}\\{ f \\} \\cdot \\mathfrak{F} \\{g\\} \\right\\}$$\n",
    "Important note: the distance of propagate is limited by sampling:\n",
    "$$z \\leq \\frac{N\\left( \\Delta x \\right)^2}{\\lambda}$$\n",
    "If the given distance to propagate is greater than the limit given by sampling the propagate will be done in multiple equidistant steps (summing to the given distance)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = 50 * u.cm\n",
    "fs_f = po.FreeSpace(f)\n",
    "wf_forward = wf * fs_f\n",
    "fig_1, ax, im = wf_forward.plot(\n",
    "    intensity=dict(cmap='gray'),\n",
    "    amplitude='default',\n",
    "    phase='default', \n",
    "    fig_options=dict(figsize=(10, 10), dpi=130)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly let's try to recreate the initial wavefront (a rectangular aperture). We should build a basic imaging system using a lens. The wavefront has already been propagated to `f`. The following must be done to recreate the image of the aperture:\n",
    "* Multiply by lens. Phase transmittance is calculated using 5.10 in Fourier Optics by J. Goodman:\n",
    "$$t_{\\text{lens}}(x, y)=\\exp \\left( -\\frac{jk}{2 f}\\left(x^{2}+y^{2}\\right)\\right)$$\n",
    "* Propagate to `2f`;\n",
    "* Multiply by another lens (of the same properties);\n",
    "* Propagate to `f`;\n",
    "Image is obtained. Let's see:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r = 2 * u.mm\n",
    "lens = po.ThinLens(2*r, f)\n",
    "fs_2f = po.FreeSpace(2*f)\n",
    "wf_multiplied = wf_forward * lens\n",
    "wf_ = wf_multiplied * fs_2f\n",
    "wf_ = wf_ * lens\n",
    "wf_ = wf_ * fs_f\n",
    "fig_1, ax, im = wf_.plot(\n",
    "    intensity='default',\n",
    "    amplitude='default',\n",
    "    phase='default', \n",
    "    fig_options=dict(figsize=(10, 10), dpi=130)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Image obtained! By using this simple 4f system we managed to image our input - a rectangular mask. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
