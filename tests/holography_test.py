import logging
import unittest
from unittest.mock import patch

import astropy.units as u
import numpy as np

import pyoptica as po
import pyoptica.holography as poh


class GerchbergSaxtonTestCase(unittest.TestCase):
    """ It is pretty difficult to actually test GS algorithm. It is difficult
    to estimate what it should return"""

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def tearDown(self):
        logging.disable(logging.NOTSET)

    def test_retrieve_phase_gs_check_return_intermediate(self):
        """We check if return_intermediate works correctly
            if return_intermediate: should return a list with wavefronts with
                len equal to max_iter
            else: should return an empty list
        """
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 100
        wf = po.Wavefront(wavelength, pixel_scale, npix)

        period = 1 * u.mm
        duty = 0.5
        angle = 45 * u.deg
        grating = po.BinaryGrating(period, duty, angle)
        target = (wf * grating).intensity
        z = 10 * u.cm
        max_iter = 3  # make it fast
        for return_intermediate, history_len in [(True, max_iter), (False, 0)]:
            with self.subTest(
                    return_intermediate=return_intermediate,
                    history_len=history_len,
            ):
                holo, history = poh.retrieve_phase_gs(
                    wf, target, z, max_iter, True, return_intermediate
                )
                self.assertEqual(history_len, len(history))

    def test_retrieve_phase_gs_check_input_wf_unchanged(self):
        """The input wavefront should not be changed (that's why we work
        on a copy
        """
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 100
        wf = po.Wavefront(wavelength, pixel_scale, npix)
        initial_phase = wf.phase
        period = 1 * u.mm
        duty = 0.5
        angle = 45 * u.deg
        grating = po.BinaryGrating(period, duty, angle)
        target = (wf * grating).intensity
        z = 10 * u.cm
        max_iter = 3
        _, _ = poh.retrieve_phase_gs(
            wf, target, z, max_iter, False, False
        )
        np.testing.assert_array_equal(wf.phase, initial_phase)

    @patch('pyoptica.Diffuser.__rmul__')
    def test_retrieve_phase_gs_phase_randomized(self, mock):
        """The input wavefront should not be changed (that's why we work
        on a copy. Only if randomize_phase = true, wavefront should be
        multiplied with Diffuser.
        """
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 100
        wf = po.Wavefront(wavelength, pixel_scale, npix)
        period = 1 * u.mm
        duty = 0.5
        angle = 45 * u.deg
        grating = po.BinaryGrating(period, duty, angle)
        target = (wf * grating).intensity
        z = 10 * u.cm
        max_iter = 3
        for randomize_phase in [False, True]:
            with self.subTest(randomize_phase=randomize_phase):
                _, _ = poh.retrieve_phase_gs(
                    wf, target, z, max_iter, randomize_phase, False
                )
                self.assertEqual(randomize_phase, mock.called)

    def test_retrieve_phase_gs_l2_norm_gets_smaller(self):
        """It's a tricky test; GS is expected to decrease the difference
        -- which we will measure using l2-norm -- after each iterations.
        In this simple test we will check if after  one iteration the norm
        decreases (I don't have a better idea).
        """
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 100
        wf = po.Wavefront(wavelength, pixel_scale, npix)
        period = 1 * u.mm
        duty = 0.5
        angle = 45 * u.deg
        grating = po.BinaryGrating(period, duty, angle)

        # To make sure energy is conserved we need to scale target instensity
        # so that total intensities at source and target are equal.
        target = (wf * grating).intensity
        energy_conservation_coef = wf.intensity.mean() / target.mean()
        target *= energy_conservation_coef

        z = 10 * u.cm
        max_iter = 1
        holo, _ = poh.retrieve_phase_gs(wf, target, z, max_iter, False, False)

        holo *= po.FreeSpace(z)
        wf *= po.FreeSpace(z)
        self.assertLessEqual(
            np.linalg.norm(holo.intensity - target),
            np.linalg.norm(wf.intensity - target)
        )


if __name__ == '__main__':
    unittest.main()
