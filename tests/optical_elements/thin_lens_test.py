import unittest
import warnings

import astropy.units as u
import numpy as np

from pyoptica.optical_elements.thin_lens import ThinLens
from pyoptica.wavefront import Wavefront

warnings.filterwarnings('error')


class TestThinLens(unittest.TestCase):
    def test_init(self):
        f = 10 * u.cm
        radius = 10 * u.cm
        lens = ThinLens(f, radius)
        self.assertEqual(lens.f, f)
        self.assertEqual(lens.radius, radius)

    def test_phase_transmittance(self):
        f = 10 * u.cm
        radius = 10 * u.cm
        lens = ThinLens(f, radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        phase_transmittance = lens.phase_transmittance(wf)
        test_set = [(250, 250, 1), (0, 0, 1), (200, 200, 1. + 1.6980327e-11j)]
        for x, y, expected in test_set:
            selected = phase_transmittance[y, x]
            with self.subTest(selected=selected, expected=expected):
                np.testing.assert_array_almost_equal(selected, expected)

    def test_amplitude_transmittance(self):
        f = 10 * u.cm
        radius = 10 * u.cm
        lens = ThinLens(f, radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude_transmittance = lens.amplitude_transmittance(wf)
        expected = np.ones((npix, npix), dtype=np.complex)
        np.testing.assert_equal(amplitude_transmittance, expected)

    def test_element_transmittance(self):
        f = 10 * u.cm
        radius = 10 * u.cm
        lens = ThinLens(f, radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude_transmittance = lens.amplitude_transmittance(wf)
        phase_transmittance = lens.phase_transmittance(wf)
        expected = amplitude_transmittance * phase_transmittance
        transmittance = lens.transmittance(wf)
        np.testing.assert_equal(transmittance, expected)

    def test_multiply_by_wavefront(self):
        f = 10 * u.mm
        radius = 10 * u.cm
        lens = ThinLens(radius, f)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        multiplied_wf = wf * lens
        expected_amp = np.ones_like(wf.amplitude)

        np.testing.assert_equal(multiplied_wf.amplitude, expected_amp)
        selected = multiplied_wf.phase[250, 250]
        np.testing.assert_equal(selected, 0)
        left_side = multiplied_wf.phase[240, 240]
        right_side = multiplied_wf.phase[260, 260]
        np.testing.assert_equal(left_side, right_side)

    def test_warns_about_bad_sampling(self):
        f = 10 * u.cm
        radius = 10 * u.cm
        lens = ThinLens(f, radius)

        wavelength = 1000 * u.nm
        lens_expected_sampling = f / (2 * radius) * wavelength
        pixel_scale = lens_expected_sampling * 2
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        with self.assertWarns(UserWarning):
            _ = wf * lens

    def test_doesnt_warn_about_good_sampling(self):
        f = 10 * u.cm
        radius = 10 * u.cm
        lens = ThinLens(f, radius)

        wavelength = 1000 * u.nm
        lens_expected_sampling = f / (2 * radius) * wavelength
        pixel_scale = lens_expected_sampling * 0.9
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        with warnings.catch_warnings(record=True) as w:
            _ = wf * lens
            if len(w) > 0 and issubclass(w[-1].category, UserWarning):
                self.fail('Warning raised unexpectedly. Sampling is OKAY!')
