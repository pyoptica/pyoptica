import unittest

import astropy.units as u
import numpy as np

from pyoptica.optical_elements.free_space import FreeSpace
from pyoptica.wavefront import Wavefront


class TestFreeSpace(unittest.TestCase):

    def test_init(self):
        distance = 10 * u.cm
        method = 'ASPW'
        fs = FreeSpace(distance, method)
        self.assertEqual(fs.distance, distance)
        self.assertEqual(fs.method, method)

    def test_calc_optimal_distance(self):
        test_set = [
            (1000 * u.nm, 50 * u.um, 1000, 2.5 * u.m),
            (100 * u.nm, 20 * u.um, 100, 0.4 * u.m),
            (633 * u.nm, 100 * u.nm, 2048, 32.35387 * u.um)
        ]
        for wl, pix_scale, npix, expected in test_set:
            with self.subTest(wl=wl, pix_scale=pix_scale, npix=npix, expected=expected):
                wf = Wavefront(wl, pix_scale, npix)
                fs = FreeSpace(20 * u.cm, 'ASPW')
                result = fs.calc_optimal_distance(wf)
                self.assertAlmostEqual(result.to(u.m).value, expected.to(u.m).value)

    def test_calc_propagation_steps(self):
        test_set = [
            (1000 * u.nm, 50 * u.um, 1000, 2.49999999 * u.m, 1, 2.5 * u.m),
            (1000 * u.nm, 50 * u.um, 1000, 3 * u.m, 2, 1.5 * u.m),
            (1000 * u.nm, 50 * u.um, 1000, 9.9999999 * u.m, 4, 2.5 * u.m),
            (100 * u.nm, 20 * u.um, 100, 0.399999999999 * u.m, 1, 0.4 * u.m),
            (633 * u.nm, 100 * u.nm, 2048, 60 * u.um, 2, 30 * u.um),
            (633 * u.nm, 100 * u.nm, 2048, -60 * u.um, 2, -30 * u.um)
        ]

        for wl, pix_scale, npix, distance, expected_number, expected_size in test_set:
            with self.subTest(wl=wl, pix_scale=pix_scale, npix=npix, distance=distance, expected_number=expected_number,
                              expected_size=expected_size):
                wf = Wavefront(wl, pix_scale, npix)
                fs = FreeSpace(distance, 'ASPW')
                number, size = fs.calc_propagation_steps(wf)
                self.assertEqual(number, expected_number)
                self.assertAlmostEqual(size.to(u.m).value, expected_size.to(u.m).value)

    def test_propagate_fresnel_forward_back(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)

        distance = 0.5 * u.m
        fs_forward = FreeSpace(distance)
        fs_back = FreeSpace(-distance)
        np.testing.assert_almost_equal(wf.wavefront, (wf * fs_forward * fs_back).wavefront)

    def test_propagate_fresnel_back_forward(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)

        distance = 0.5 * u.m
        fs_back = FreeSpace(-distance)
        fs_forward = FreeSpace(distance)
        np.testing.assert_almost_equal(wf.wavefront, (wf * fs_back * fs_forward).wavefront)


if __name__ == '__main__':
    unittest.main()
