import unittest

import astropy.units as u
import numpy as np

from pyoptica.optical_elements.diffuser import Diffuser
from pyoptica.wavefront import Wavefront


class TestDiffuser(unittest.TestCase):
    def test_init(self):
        distribution = 'uniform'
        rotating = False
        diffuser = Diffuser(distribution, rotating)
        self.assertEqual(diffuser.distribution, distribution)

    def test_reset_seed(self):
        distribution = 'uniform'
        rotating = False
        diffuser = Diffuser(distribution, rotating)
        diffuser._reset_seed()
        arr_1_1 = diffuser._rnd_state.rand(10)
        arr_1_2 = diffuser._rnd_state.rand(10)
        diffuser._reset_seed()
        arr_2_1 = diffuser._rnd_state.rand(10)
        arr_2_2 = diffuser._rnd_state.rand(10)
        np.testing.assert_array_equal(arr_1_1, arr_2_1)
        np.testing.assert_array_equal(arr_1_2, arr_2_2)

    def test_amplitude_transmittance(self):
        distribution = 'uniform'
        rotating = False
        diffuser = Diffuser(distribution, rotating)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude_transmittance = diffuser.amplitude_transmittance(wf)
        test_set = [(250, 250, 1), (0, 0, 1), (150, 250, 1), (250, 150, 1)]
        for x, y, expected in test_set:
            selected = amplitude_transmittance[y, x]
            with self.subTest(selected=selected, expected=expected):
                np.testing.assert_array_almost_equal(selected, expected)

    def test_phase_transmmitance(self):
        distribution = 'uniform'
        rotating_1 = True
        rotating_2 = False
        diffuser_1 = Diffuser(distribution, rotating_1)
        diffuser_2 = Diffuser(distribution, rotating_2)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)

        phase_1_1_spin = diffuser_1.phase_transmittance(wf)
        phase_1_2_spin = diffuser_1.phase_transmittance(wf)

        phase_2_1_spin = diffuser_2.phase_transmittance(wf)
        phase_2_2_spin = diffuser_2.phase_transmittance(wf)

        np.testing.assert_equal(np.any(np.not_equal(phase_1_1_spin, phase_1_2_spin)), True)
        np.testing.assert_array_equal(phase_2_1_spin, phase_2_2_spin)

    def test_element_transmittance(self):
        distribution = 'uniform'
        rotating = False
        diffuser = Diffuser(distribution, rotating)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude_transmittance = diffuser.amplitude_transmittance(wf)
        phase_transmittance = diffuser.phase_transmittance(wf)
        expected = amplitude_transmittance * phase_transmittance
        transmittance = diffuser.transmittance(wf)
        np.testing.assert_equal(transmittance, expected)

    def test_multiply_by_wavefront(self):
        distribution = 'uniform'
        rotating = False
        diffuser = Diffuser(distribution, rotating)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)

        multiplied_left_wf = wf * diffuser
        multiplied_right_wf = diffuser * wf

        np.testing.assert_equal(
            multiplied_left_wf.wavefront, multiplied_right_wf.wavefront
        )
