import unittest

import astropy.units as u
import numpy as np

from pyoptica.optical_elements.binary_grating import BinaryGrating
from pyoptica.wavefront import Wavefront


class TestBinaryGrating(unittest.TestCase):
    def test_init(self):
        period = 200 * u.um
        duty = .5
        angle = 45 * u.deg
        grating = BinaryGrating(period, duty, angle)
        self.assertEqual(grating.period, period)
        self.assertEqual(grating.duty, duty)
        self.assertEqual(grating.angle, angle)

    def test_etch_grating(self):
        npix = 512
        period = 32
        duty = .5
        angle = 0
        etched_grating = BinaryGrating.etch_grating(npix, period, duty, angle)
        # print(etched_grating[npix//2, npix//2 - 8])
        test_set = [(etched_grating[npix // 2, npix // 2 - 8], 0),
                    (etched_grating[npix // 2, npix // 2 + 8], 1),
                    (np.mean(etched_grating), .5)
                    ]
        for selected, expected in test_set:
            with self.subTest(selected=selected, expected=expected):
                self.assertEqual(selected, expected)

    def test_amplitude_transmittance(self):
        wavelength = 1000 * u.nm
        npix = 512
        pix_scale = 100 * u.um
        wf = Wavefront(wavelength, pix_scale, npix)
        period = 3.2 * u.mm
        duty = .5
        angle = 45 * u.deg
        gr = BinaryGrating(period, duty, angle)
        amp = (wf * gr).amplitude
        phs = (wf * gr).phase
        test_set = [(amp[npix // 2, npix // 2 - 8], 0),
                    (amp[npix // 2, npix // 2 + 8], 1),
                    (amp[npix // 2 + 8, npix // 2], 0),
                    (amp[npix // 2 - 8, npix // 2], 1),
                    (np.mean(phs), 0)
                    ]
        for selected, expected in test_set:
            with self.subTest(selected=selected, expected=expected):
                self.assertEqual(selected, expected)

    def test_phase_transmittance(self):
        wavelength = 1000 * u.nm
        npix = 512
        pix_scale = 100 * u.um
        wf = Wavefront(wavelength, pix_scale, npix)
        period = 3.2 * u.mm
        duty = .5
        angle = 45 * u.deg
        gr = BinaryGrating(period, duty, angle, kind='phase')
        amp = (wf * gr).amplitude
        phs = (wf * gr).phase
        test_set = [(np.mean(amp), 1),
                    (phs[npix // 2, npix // 2 - 8], 0),
                    (phs[npix // 2, npix // 2 + 8], np.pi),
                    (phs[npix // 2 + 8, npix // 2], 0),
                    (phs[npix // 2 - 8, npix // 2], np.pi)
                    ]
        for selected, expected in test_set:
            with self.subTest(selected=selected, expected=expected):
                self.assertEqual(selected, expected)
