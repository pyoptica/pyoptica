import unittest

import astropy.units as u
import numpy as np

from pyoptica import zernikes, utils


class TestZernikes(unittest.TestCase):
    def _get_normalized_coords(
            self, npix=100, pixel_scale=0.5 * u.m, make_disk=True
    ):
        x, y = utils.mesh_grid(npix, pixel_scale)
        r_max = .5 * npix * pixel_scale
        r, theta = utils.cart2pol(x, y)
        if make_disk:
            r[r > r_max] = 0 * pixel_scale
        r = r / r_max
        return r.value, theta.value

    def test_remove_units(self):
        x, y = utils.mesh_grid(100, 0.5 * u.m)
        r_max = .5 * 100 * 0.5 * u.m
        r, theta = utils.cart2pol(x, y)
        r = r / r_max
        z = zernikes._zernike_mn(1, 1, r, theta)
        self.assertNotIsInstance(z, u.Quantity)

    def test_radial_function(self):
        #  I am using Table 1. from:
        #  Vasudevan Lakshminarayanan and Andre Fleck (2011)
        #  *Zernike polynomials: a guide*, Journal of Modern Optics
        #   (n, m, (polynomial coeficients)

        r, _ = self._get_normalized_coords()
        test_set = [
            (1, -1, (1, 0)),
            (1, 1, (1, 0)),
            (5, 5, (1, 0, 0, 0, 0, 0)),
            (3, -3, (1, 0, 0, 0)),
            (3, 1, (3, 0, -2, 0)),
            (7, 7, (1, 0, 0, 0, 0, 0, 0, 0))
        ]
        for n, m, poly_coefs in test_set:
            with self.subTest(m=m, n=n, poly_coefs=poly_coefs):
                actual = zernikes.R(m, n, r)
                expected = np.poly1d(poly_coefs)(r)
                np.testing.assert_array_almost_equal(actual, expected)

    def test_radial_function_1_at_edge(self):
        #  Rn+-m(1) = 1
        r, _ = self._get_normalized_coords()
        edge_indices = np.where(r == 1)
        expected = 1
        for m, n in [(0, 0), (-1, 1), (1, 1), (-4, 10), (10, 10), (1, 7)]:
            with self.subTest(m=m, n=n):
                actual = zernikes.R(m, n, r)[edge_indices]
                np.testing.assert_equal(actual, expected)

    def test_norm_coefficient(self):
        test_set = [
            (0, 0, 1), (1, 11, 24 ** 0.5), (2, 12, 26 ** 0.5),
            (0, 12, 13 ** 0.5),
            (7, 15, 32 ** 0.5), (19, 99, 200 ** 0.5), (0, 99, 100 ** 0.5)
        ]
        for m, n, expected in test_set:
            with self.subTest(m=m, n=n, expected=expected):
                actual = zernikes.norm_coefficient(m, n)
                self.assertEqual(actual, expected)

    def test_zernikes(self):
        #  I am using Table 1. from:
        #  Vasudevan Lakshminarayanan and Andre Fleck (2011)
        #  *Zernike polynomials: a guide*, Journal of Modern Optics
        #   (n, m, (polynomial)(r) * trig(theta))

        r, theta = self._get_normalized_coords(make_disk=False)
        r_p = np.where(r <= 1, r, 0)
        flat_disk = np.where(r <= 1, 1, 0)
        test_set = [
            (5, 3, np.poly1d([5, 0, -4, 0, 0, 0])(r_p) * np.cos(3 * theta)),
            (6, -6, np.poly1d([1, 0, 0, 0, 0, 0, 0])(r_p) * np.sin(6 * theta)),
            (2, 0, np.poly1d([2, 0, -1])(r_p)),
            (7, 1,
             np.poly1d([35, 0, -60, 0, 30, 0, -4, 0])(r_p) * np.cos(theta)),
            (1, 1, np.poly1d([1, 0])(r_p) * np.cos(theta)),
        ]
        for n, m, expected in test_set:
            with self.subTest(m=m, n=n, expected=expected):
                actual = zernikes._zernike_mn(m, n, r, theta, normalize=False)
                np.testing.assert_array_almost_equal(actual,
                                                     expected * flat_disk)

    def test_zernike_fill_value(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        z = zernikes._zernike_mn(-1, 5, r, theta, fill_value=np.nan)
        self.assertTrue(np.isnan(z[0, 0]))

    def test_zernike_raises_n_smaller_than_0(self):
        m = -1
        n = -1
        msg = f"Radial order n = {n} < 0! Must be n > 0."
        with self.assertRaisesRegex(ValueError, msg):
            zernikes._zernike_mn(m, n, None, None)

    def test_zernike_raises_m_greater_than_n(self):
        m = 10
        n = 9
        msg = f"Radial order n > m angular frequency: {n} > {m}."
        with self.assertRaisesRegex(ValueError, msg):
            zernikes._zernike_mn(m, n, None, None)

    def test_zernike_raises_m_n_difference_not_even(self):
        m = 8
        n = 9
        msg = f"Radial order n - m angular frequency is not even: " \
            f"{n} - {m} = {n - m}"
        with self.assertRaisesRegex(ValueError, msg):
            zernikes._zernike_mn(m, n, None, None)

    def test_osa_to_mn(self):
        # j, m, n
        test_set = [(0, 0, 0), (4, 0, 2), (6, -3, 3), (5, 2, 2), (9, 3, 3)]
        for j, m, n in test_set:
            with self.subTest(j=j, m=m, n=n):
                actual = zernikes.osa_to_mn(j)
                self.assertTupleEqual(actual, (m, n))

    def test_mn_to_osa(self):
        test_set = [(0, 0, 0), (4, 0, 2), (6, -3, 3), (5, 2, 2), (9, 3, 3)]
        for j, m, n in test_set:
            with self.subTest(j=j, m=m, n=n):
                actual = zernikes.mn_to_osa(m, n)
                self.assertEqual(actual, j)

    def test_noll_to_mn(self):
        # j, m, n
        test_set = [
            (1, 0, 0), (2, 1, 1), (3, -1, 1), (4, 0, 2), (5, -2, 2), (6, 2, 2),
            (15, -4, 4), (16, 1, 5), (17, -1, 5), (18, 3, 5), (19, -3, 5)
        ]
        for j, m, n in test_set:
            with self.subTest(j=j, m=m, n=n):
                actual = zernikes.noll_to_mn(j)
                self.assertTupleEqual(actual, (m, n))

    def test_mn_to_noll(self):
        test_set = [
            (1, 0, 0), (2, 1, 1), (3, -1, 1), (4, 0, 2), (5, -2, 2), (6, 2, 2),
            (15, -4, 4), (16, 1, 5), (17, -1, 5), (18, 3, 5), (19, -3, 5)
        ]
        for j, m, n in test_set:
            with self.subTest(j=j, m=m, n=n):
                actual = zernikes.mn_to_noll(m, n)
                self.assertEqual(actual, j)

    def test_fringe_to_mn(self):
        # j, m, n
        test_set = [
            (1, 0, 0), (2, 1, 1), (3, -1, 1), (4, 0, 2), (5, 2, 2), (6, -2, 2),
            (15, -1, 5), (16, 0, 6), (17, 4, 4), (18, -4, 4), (19, 3, 5),
            (128, 8, 14), (55, -5, 9)
        ]
        for j, m, n in test_set:
            with self.subTest(j=j, m=m, n=n):
                actual = zernikes.fringe_to_mn(j)
                self.assertTupleEqual(actual, (m, n))

    def test_mn_to_fringe(self):
        test_set = [
            (1, 0, 0), (2, 1, 1), (3, -1, 1), (4, 0, 2), (5, 2, 2), (6, -2, 2),
            (15, -1, 5), (16, 0, 6), (17, 4, 4), (18, -4, 4), (19, 3, 5),
            (128, 8, 14), (55, -5, 9)
        ]
        for j, m, n in test_set:
            with self.subTest(j=j, m=m, n=n):
                actual = zernikes.mn_to_fringe(m, n)
                self.assertEqual(actual, j)

    def test_to_mn(self):
        test_set = [  # j, convention, m, n
            (7, 'noll', -1, 3), (7, 'osa', -1, 3), (7, 'fringe', 1, 3),
            (33, 'noll', -5, 7), (33, 'osa', 3, 7), (33, 'fringe', -2, 8)
        ]
        for j, convention, m, n in test_set:
            with self.subTest(j=j, convention=convention, m=m, n=n):
                actual = zernikes._to_mn(j, convention)
                self.assertEqual((m, n), actual)

    def test_to_mn_raises(self):
        with self.assertRaises(ValueError):
            _ = zernikes._to_mn(42, 'This cannot work')

    def test_construct_wavefront(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        test_set = [
            ({1: 1}, 'noll', zernikes.zernike(1, 'noll', r, theta)),
            ({1: 1}, 'osa', zernikes.zernike(1, 'osa', r, theta)),
            ({1: 1}, 'fringe', zernikes.zernike(1, 'fringe', r, theta)),
            ({1: 0}, 'osa', np.zeros_like(r)),
            ({(0, 0): 1}, 'mn', zernikes.zernike((0, 0), 'mn', r, theta)),
            (dict(zip([1, 2, 3, 4, 5], [0.1] * 5)), 'noll', 0.1 * sum(
                [zernikes.zernike(j, 'noll', r, theta) for j in
                 [1, 2, 3, 4, 5]])),
            (dict(zip([1, 2, 3, 4, 5], [0.1] * 5)), 'osa', 0.1 * sum(
                [zernikes.zernike(j, 'osa', r, theta) for j in
                 [1, 2, 3, 4, 5]])),
            (dict(zip([1, 2, 3, 4, 5], [0.1] * 5)), 'fringe', 0.1 * sum(
                [zernikes.zernike(j, 'fringe', r, theta) for j in
                 [1, 2, 3, 4, 5]])),

        ]
        for js_coefs, convention, expected in test_set:
            with self.subTest(js_coefs=js_coefs, convention=convention,
                              expected=expected):
                actual = zernikes.construct_wavefront(
                    js_coefs, convention, r, theta)
                np.testing.assert_array_almost_equal(actual, expected)

    def test_fit_zernikes_raises(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        coefs = np.ones((10,)) * 0.1
        js = [4, 5, 6, 7, 8, 9, 10, 21, 22, 30]
        js_coefs = dict(zip(js, coefs))
        wf = zernikes.construct_wavefront(js_coefs, 'noll', r, theta,
                                          fill_value=np.nan)
        with self.assertRaises(ValueError):
            _ = zernikes.fit_zernikes(wf, js, 'noll', r, theta, cache=True)

    def test_fit_zernikes(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        convention = 'osa'
        coefs = np.ones((10,)) * 0.1
        js = [4, 5, 6, 7, 8, 9, 10, 21, 22, 30]
        js_coefs = dict(zip(js, coefs))
        wf = zernikes.construct_wavefront(js_coefs, convention, r, theta)
        for cache in [True, False]:
            with self.subTest(cache=cache):
                fit, res = zernikes.fit_zernikes(wf, js, convention, r, theta,
                                                 cache=cache)
                fitted_coefs = np.array(list(fit.values()))
                np.testing.assert_array_almost_equal(
                    coefs, fitted_coefs, decimal=4)

    def test_fit_zernikes_lstsq(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        coefs = np.ones((10,)) * 0.1
        js = [4, 5, 6, 7, 8, 9, 10, 21, 22, 30]
        js_coefs = dict(zip(js, coefs))
        convention = 'noll'
        wf = zernikes.construct_wavefront(js_coefs, convention, r, theta)
        for cache in [True, False]:
            with self.subTest(cache=cache):
                fit, res = zernikes.fit_zernikes_lstsq(wf, js, convention, r,
                                                       theta)
                fitted_coefs = np.array(list(fit.values()))
                np.testing.assert_array_almost_equal(
                    coefs, fitted_coefs, decimal=4)

    def test_l2_norm_of_aberration_wavefront_cache(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        z1 = zernikes.zernike(1, 'osa', r, theta)
        five_j_wf = zernikes.construct_wavefront(
            dict(zip([1, 2, 3, 4, 5], [1] * 5)), 'osa', r, theta, True)
        five_j_wf_norm = np.linalg.norm(five_j_wf)
        test_set = [
            ([1], z1, [z1], 0),
            ([0], z1, [z1], np.linalg.norm(z1)),
            ([1, 2, 3, 4, 5], five_j_wf, [np.zeros_like(r) for _ in range(5)],
             five_j_wf_norm)
        ]
        for coefs, wf, cached, expected in test_set:
            with self.subTest(cached=cached, wf=wf, coefs=coefs,
                              expected=expected):
                actual = zernikes._l2_norm_of_aberration_wavefront_cache(
                    coefs, wf, cached)
                self.assertAlmostEqual(actual, expected)

    def test_l2_norm_of_aberration_wavefront(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        convention = 'osa'
        five_j_wf = zernikes.construct_wavefront(
            dict(zip([1, 2, 3, 4, 5], [1] * 5)), convention, r, theta, True)
        five_j_wf_norm = np.linalg.norm(five_j_wf)
        z1 = zernikes.zernike(1, convention, r, theta)
        test_set = [
            ([1], [1], z1, 0),
            ([1], [0], z1, np.linalg.norm(z1)),
            ([1, 2, 3, 4, 5], [1] * 5, np.zeros_like(r), five_j_wf_norm)
        ]
        for js, coefs, wf, expected in test_set:
            with self.subTest(js=js, coefs=coefs, expected=expected, wf=wf):
                actual = zernikes._l2_norm_of_aberration_wavefront(
                    coefs, js, 'osa', wf, r, theta, True)
                self.assertAlmostEqual(actual, expected)

    def test_zernike_raises_bad_convention(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        with self.assertRaises(ValueError):
            _ = zernikes.zernike(5, 'YOLO', r, theta)

    def test_zernike_correct_convention(self):
        r, theta = self._get_normalized_coords(make_disk=False)
        m, n = -4, 4
        osa = 10
        noll = 15
        fringe = 18
        expected = zernikes._zernike_mn(m, n, r, theta, True)
        test_set = [
            ((m, n), 'mn'), (osa, 'osa'), (noll, 'noll'), (fringe, 'fringe')
        ]
        for index, convention in test_set:
            with self.subTest(index=index, convention=convention):
                actual = zernikes.zernike(index, convention, r, theta)
                np.testing.assert_array_equal(expected, actual)

    def test_get_zernike_name(self):
        test_set = [  # index, convention, latex, expected
            ((0, 0), 'mn', True, '$Z_{0}^{0}$ Piston'),
            (1, 'noll', True, '$Z_{1}$ Piston'),
            (1, 'fringe', True, '$Z_{1}$ Piston'),
            (0, 'osa', True, '$Z_{0}$ Piston'),
            (7, 'noll', True, '$Z_{7}$ y-Coma'),
            (50, 'noll', True, '$Z_{50}$'),
            (1500, 'osa', True, '$Z_{1500}$'),
            (100, 'osa', True, '$Z_{100}$'),
            ((3, 3), 'mn', True, '$Z_{3}^{3}$ x-Trefoil'),
            (8, 'fringe', True, '$Z_{8}$ y-Coma'),
            ((0, 0), 'mn', False, 'Z_0^0 Piston'),
            (1, 'noll', False, 'Z_1 Piston'),
            (1, 'fringe', False, 'Z_1 Piston'),
            (0, 'osa', False, 'Z_0 Piston'),
            (7, 'noll', False, 'Z_7 y-Coma'),
            (50, 'noll', False, 'Z_50'),
            (1500, 'osa', False, 'Z_1500'),
            (100, 'osa', False, 'Z_100'),
            ((3, 3), 'mn', False, 'Z_3^3 x-Trefoil'),
            (8, 'fringe', False, 'Z_8 y-Coma'),
        ]
        for index, convention, latex, expected in test_set:
            with self.subTest(
                    index=index, convention=convention, latex=latex,
                    expected=expected
            ):
                actual = zernikes.get_zernike_name(index, convention, latex)
                self.assertEqual(expected, actual)

    def test_get_zernike_name_raises_bad_convention(self):
        with self.assertRaises(ValueError):
            _ = zernikes.get_zernike_name(5, 'YOLO')
