import unittest

from pyoptica import distribution


class TestBaseOpticalElement(unittest.TestCase):
    def test_is_abstract(self):
        with self.assertRaises(TypeError):
            _ = distribution.Distribution()
