import unittest

import pyoptica.logging as pol


class TestLogging(unittest.TestCase):
    def test_inheritance_from_mixin(self):
        class ChildTestClass(pol.LoggerMixin):
            pass

        test_object = ChildTestClass()
        expected = 'ChildTestClass'
        actual = test_object.logger.name.split('.')[-1]  # let's skip module
        self.assertEqual(expected, actual)

    def test_get_standard_logger(self):
        name = 'my_test_name'
        logger = pol.get_standard_logger(name)
        self.assertEqual(name, logger.name)


if __name__ == '__main__':
    unittest.main()
