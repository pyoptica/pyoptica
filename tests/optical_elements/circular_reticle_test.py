import unittest

import astropy.units as u
import numpy as np

from pyoptica.optical_elements.circular_reticle import CircularAperture, CircularObscuration
from pyoptica.wavefront import Wavefront


class TestCircularAperture(unittest.TestCase):
    def test_init(self):
        radius = 10 * u.cm
        aperture = CircularAperture(radius)
        self.assertEqual(aperture.radius, radius)

    def test_phase_transmittance(self):
        radius = 10 * u.cm
        aperture = CircularAperture(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)
        phase_transmittance = aperture.phase_transmittance(wf)
        test_set = [(250, 250, np.exp(0)), (0, 0, np.exp(0)), (150, 250, np.exp(0)), (250, 150, np.exp(0))]
        for x, y, expected in test_set:
            selected = phase_transmittance[y, x]
            with self.subTest(selected=selected, expected=expected):
                np.testing.assert_array_almost_equal(selected, expected)

    def test_amplitude_transmittance(self):
        radius = 10 * u.cm
        aperture = CircularAperture(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)
        phase_transmittance = aperture.amplitude_transmittance(wf)
        test_set = [(250, 250, 1), (0, 0, 0), (150, 250, 1), (250, 150, 1)]
        for x, y, expected in test_set:
            selected = phase_transmittance[y, x]
            with self.subTest(selected=selected, expected=expected):
                np.testing.assert_array_almost_equal(selected, expected)

    def test_element_transmittance(self):
        radius = 10 * u.cm
        aperture = CircularAperture(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude_transmittance = aperture.amplitude_transmittance(wf)
        phase_transmittance = aperture.phase_transmittance(wf)
        expected = amplitude_transmittance * phase_transmittance
        transmittance = aperture.transmittance(wf)
        np.testing.assert_equal(transmittance, expected)

    def test_multiply_by_wavefront(self):
        radius = 10 * u.cm
        aperture = CircularAperture(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)

        multiplied_left_wf = wf * aperture
        multiplied_right_wf = aperture * wf

        np.testing.assert_equal(
            multiplied_left_wf.wavefront, multiplied_right_wf.wavefront
        )


class TestCircularObscuration(unittest.TestCase):
    def test_init(self):
        radius = 10 * u.cm
        obscuration = CircularObscuration(radius)
        self.assertEqual(obscuration.radius, radius)

    def test_phase_transmittance(self):
        radius = 10 * u.cm
        obscuration = CircularObscuration(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)
        phase_transmittance = obscuration.phase_transmittance(wf)
        test_set = [(250, 250, np.exp(0)), (0, 0, np.exp(0)), (150, 250, np.exp(0)), (250, 150, np.exp(0))]
        for x, y, expected in test_set:
            selected = phase_transmittance[y, x]
            with self.subTest(selected=selected, expected=expected):
                np.testing.assert_array_almost_equal(selected, expected)

    def test_amplitude_transmittance(self):
        radius = 10 * u.cm
        obscuration = CircularObscuration(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500

        wf = Wavefront(wavelength, pixel_scale, npix)
        phase_transmittance = obscuration.amplitude_transmittance(wf)
        test_set = [(250, 250, 0), (0, 0, 1), (150, 250, 0), (250, 150, 0)]
        for x, y, expected in test_set:
            selected = phase_transmittance[y, x]
            with self.subTest(selected=selected, expected=expected):
                np.testing.assert_array_almost_equal(selected, expected)

    def test_element_transmittance(self):
        radius = 10 * u.cm
        obscuration = CircularObscuration(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude_transmittance = obscuration.amplitude_transmittance(wf)
        phase_transmittance = obscuration.phase_transmittance(wf)
        expected = amplitude_transmittance * phase_transmittance
        transmittance = obscuration.transmittance(wf)
        np.testing.assert_equal(transmittance, expected)

    def test_multiply_by_wavefront(self):
        radius = 10 * u.cm
        obscuration = CircularObscuration(radius)

        wavelength = 1000 * u.nm
        pixel_scale = 1 * u.mm
        npix = 500
        wf = Wavefront(wavelength, pixel_scale, npix)

        multiplied_left_wf = wf * obscuration
        multiplied_right_wf = obscuration * wf

        np.testing.assert_equal(
            multiplied_left_wf.wavefront, multiplied_right_wf.wavefront
        )
