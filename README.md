<p align="center">
  <img src="https://gitlab.com/pyoptica/pyoptica/-/raw/master/pyoptica/data/logos/pyoptica_round.png" width="150" title="PyOptica Logo Round">
</p>


# PyOptica

PyOptica is a package for simulation of wave optics in Python. It is developed to deal with optics simulations in a *pythonic* way; it is one of the most important presupposition of the whole project to follow the Zen of Python and create a structure that is known to users from the most popular scientific packages: NumPy or SciPy.

## Blog
Please visit our blog on: [https://pyoptica.gitlab.io/pyoptica-blog/](https://pyoptica.gitlab.io/pyoptica-blog/)

## The Goal

The package is meant to provide functionality to propagate a wavefront through space, interact with optical elements (e.g. lenses or apertures), build optical systems, and to implement basic algorithms for holography. As I am writing these words not everything listed above has already been developed, however, we are doing are best to develop everything. Of course we would also like to invite **YOU** to the development of the package. Everybody is welcome –  both experienced developers or optical engineers and students who have just embarked on their adventure with optics.

Not only are we developing a software package but also we are committed to work on a series of Notebooks that would solve problems described in the most popular books on Optics. There is also a room for potential expansion of what is developed: you use the package to verify that your solution of a given problem is correct? Make a notebook out of it!

## Documentation

Documentation can be found [here](https://pyoptica.gitlab.io/pyoptica/).


## About Us

Our names are Maciej Grochowicz and Michał Miler. I guess our names may sound unfamiliar to you – we are from Poland where we also studied Applied Physics at Warsaw University of Technology. Right now we are professional engineers. In case you would like to learn more about us, please visit our Linkedin profiles:

- [Maciej Grochowicz](https://www.linkedin.com/in/maciej-grochowicz-b70a72b4/)
- [Michał Miler](https://www.linkedin.com/in/milerm/)



## Literature

Although we would love to say that we relied solely on our knowledge and experience, we are obliged to list all of the books we used during the development:

1. Joseph W. Goodman (2004) *Introduction to Fourier Optics*, W. H. Freeman",
2. Kedar Khare (2016) *Fourier Optics and Computational Imaging*, Wiley&Sons Ltd.",
3. David Voelz (2011) *Computational Fourier Optics Matlab Tutorial*, Spie Press"

