{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"../pyoptica/data/logos/pyoptica_round.png\" alt=\"PyOptica Logo\" width=\"100\"/>\n",
    "\n",
    "# Zernike Polynomials\n",
    "The Zernike polynomials are a sequence of polynomials that are orthogonal over a unit disk. They are named after Dutch physicist Frits Zernike (winner of the Nobel Prize in 1953). A lot of modern imaging systems use circular elements (and pupils), as a result, optical wavefronts (phase distributions) propagating through such systems are described using Zernike polynomials. Entire theoretical introduction as well as software implementation are based on [*Zernike polynomials: a guide*](https://www.researchgate.net/publication/241585467_Zernike_polynomials_A_guide) (which is freely available on researchgate!) [1], [zernike polynomials on wikipedia](https://en.wikipedia.org/wiki/Zernike_polynomials) [2]. \n",
    "\n",
    "How can we construct a wavefront from zernikes? Firstly of all, entire mathematical analysis is carried on in polar coordinates $(r, \\theta)$. We consider a wavefront $W(r, \\theta)$ that can be constructed from a sequence of polynomials $Z^m_n$ with corresponding coefficients $C^m_n$:\n",
    "\n",
    "\\begin{equation}\n",
    "W(r, \\theta)=\\sum_{n, m} C_{n}^{m} Z_{n}^{m}(r, \\theta),\n",
    "\\end{equation}\n",
    "with indices $m $ -- angular frequency, $n$ -- radial order. $Z^m_n(r, \\theta)$  (where $r \\leq 1$) are defined as: \n",
    "\\begin{equation}\n",
    "\\begin{aligned} Z_{n}^{m}(r, \\theta) &=R_{n}^{m}(r) \\cos m \\theta \\quad \\text { for } m \\geq 0, \\\\ Z_{n}^{-m}(r, \\theta) &=R_{n}^{m}(r) \\sin m \\theta \\quad \\text { for } m<0. \\end{aligned}\n",
    "\\end{equation}\n",
    "\n",
    "Lastly we have to define the radial function $R_{n}^{m}(r)$:\n",
    "\\begin{equation}\n",
    "R_{n}^{m}(r)=\\sum_{l=0}^{(n-m) / 2} \\frac{(-1)^{l}(n-l) !}{l !\\left[\\frac{1}{2}(n+m)-l\\right] !\\left[\\frac{1}{2}(n-m)-l\\right] !} r^{n-2 l}\n",
    "\\end{equation}\n",
    "The radial function can normalized so that $R_{n}^{ \\pm m}(1) = 1$ with:\n",
    "\\begin{equation}\n",
    "N_{n}^{m}=\\left(\\frac{2(n+1)}{1+\\delta_{m 0}}\\right)^{1 / 2},\n",
    "\\end{equation}\n",
    "where $\\delta_{m 0}=0$ for $m \\neq 0$.\n",
    "\n",
    "### Indices\n",
    "\n",
    "Last thing we need to discuss is the indexing. We have already defined $m$, the angular frequency and $n$, the radial order which are used to define Zernike polynomials. There are also other indexing schemes using just one index $j$ instead of two... however, there is many different definitions (just visit the wikipedia page).\n",
    "\n",
    "OSA/ANSI standard indices:\n",
    "\\begin{equation}\n",
    "j=\\frac{n(n+2)+m}{2}\n",
    "\\end{equation}\n",
    "\n",
    "Noll's sequential indices:\n",
    "\\begin{equation}\n",
    "j=\\frac{n(n+1)}{2}+|m|+\\left\\{\\begin{array}{l}{0, \\quad m>0 \\wedge n \\equiv\\{0,1\\}(\\bmod 4)} \\\\ {0, \\quad m<0 \\wedge n \\equiv\\{2,3\\}(\\bmod 4)} \\\\ {1, \\quad m \\geq 0 \\wedge n \\equiv\\{2,3\\}(\\bmod 4)} \\\\ {1, \\quad m \\leq 0 \\wedge n \\equiv\\{0,1\\}(\\bmod 4)}\\end{array}\\right.\n",
    "\\end{equation}\n",
    "\n",
    "Fringe/University of Arizona indices:\n",
    "\\begin{equation}\n",
    "j=\\left(1+\\frac{n+|m|}{2}\\right)^{2}-2|m|+\\frac{1-\\operatorname{sgn} m}{2}\n",
    "\\end{equation}\n",
    "\n",
    "**In PyOptica all three (Noll, OSA, and Fringe) are available.** Zen of Python says: *Explicit is better than implicit*, thus, to avoid confusion (which indexing surely causes) we ask you to always explicitly tell us which convention you use!\n",
    "\n",
    "### One More Thing\n",
    "\n",
    "**Really** last thing, in [1] the angle used in zernikes is defined as the angle between $r$ and $OY$-axis. In the examples provided in this notebook, however, we are going to used the standard definition -- the angle between $r$ and $OX$-axis.\n",
    "\n",
    "More details can be found in the literature. Information regarding the influence of aberrations on imaging  can be found in any textbook on optics.\n",
    "## Literature\n",
    "1. [Lakshminarayanan, Vasudevan & Fleck, Andre. (2011). *Zernike polynomials: A guide.* Journal of Modern Optics - J MOD OPTIC. 58. 1678-1678. 10.1080/09500340.2011.633763.](https://www.researchgate.net/publication/241585467_Zernike_polynomials_A_guide)\n",
    "2. [Zernike polynomials on Wikipedia](https://en.wikipedia.org/wiki/Zernike_polynomials)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Some Examples\n",
    "Let's start with looking at some examples of Zernike polynomials: coma `m, n = (-1, 3)`. When creating the input data (`r` and `theta`) we have to remember that zernikes are defined over a unit disk. You can compare the results with examples on [wikipedia](https://en.wikipedia.org/wiki/Zernike_polynomials#/media/File:Zernike_polynomials2.png)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import astropy.units as u\n",
    "import numpy as np\n",
    "\n",
    "import pyoptica as po\n",
    "\n",
    "\n",
    "npix = 255\n",
    "pixel_scale = 0.5 * u.m\n",
    "x, y = po.utils.mesh_grid(npix, pixel_scale)\n",
    "r_max = .48 * npix * pixel_scale\n",
    "r, theta = po.utils.cart2pol(x, y)\n",
    "r = r / r_max\n",
    "\n",
    "m, n = -1, 3\n",
    "z = po.zernike((m, n), 'mn', r, theta, fill_value=np.nan)\n",
    "name = po.zernikes.get_zernike_name((m, n), 'mn')\n",
    "_ = po.plotting.plot_zernike(z, r, theta, title=name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now try with an OSA index."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "j = po.zernikes.mn_to_osa(m, n)\n",
    "z_j = po.zernike(j, 'osa', r, theta, fill_value=np.nan)\n",
    "name = po.zernikes.get_zernike_name(j, 'osa')\n",
    "_ = po.plotting.plot_zernike(z_j, r, theta, title=name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With Noll index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "j = po.zernikes.mn_to_noll(m, n)\n",
    "z_j = po.zernike(j, 'noll', r, theta, fill_value=np.nan)\n",
    "name = po.zernikes.get_zernike_name(j, 'noll')\n",
    "_ = po.plotting.plot_zernike(z_j, r, theta, title=name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And lastly with Fringe index (here the index is different... but the distribution stays as expected!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "j = po.zernikes.mn_to_fringe(m, n)\n",
    "z_j = po.zernike(j, 'fringe', r, theta, fill_value=np.nan)\n",
    "name = po.zernikes.get_zernike_name(j, 'fringe')\n",
    "_ = po.plotting.plot_zernike(z_j, r, theta, title=name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wavefront Fitting\n",
    "Zernikes are often used to fit wavefronts -- when we deal with circular symmetry of course. We are trying to find a set of coefficients $c_j$ that will give us $W(R, \\theta)$:\n",
    "\\begin{equation}\n",
    "W(r, \\theta)=\\sum_{n, m} C_j Z_j(r, \\theta).\n",
    "\\end{equation}\n",
    "\n",
    "PyOptica provides the functionality for wavefront fitting. For the given wavefront $W(r, \\theta)$ (please note that the wavefront is a `numpy.array` not `pyoptica.Wavefront`!) it tries to find coefficients $C_j$ using the least squares method to solve: \n",
    "\\begin{equation}\n",
    "Z_j \\cdot C_j = W\n",
    "\\end{equation}\n",
    "**Please note that this is vector multiplication!**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(1337)\n",
    "\n",
    "coefs = (np.random.rand(30) - 0.5) / 10\n",
    "js = list(range(len(coefs)))\n",
    "js_coefs = dict(zip(js, coefs))\n",
    "wf = po.zernikes.construct_wavefront(js_coefs, 'osa', r, theta, fill_value=0) #  We cannot use nans in fitting procedure\n",
    "_ = po.plotting.plot_zernike(wf, r, theta)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fit_js, res = po.zernikes.fit_zernikes_lstsq(wf, js, 'osa', r, theta)\n",
    "wf_from_fit = po.zernikes.construct_wavefront(fit_js, 'osa', r, theta, fill_value=0)\n",
    "_ = po.plotting.plot_zernike(wf_from_fit, r, theta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's checkout the difference (note that the range is completely different!):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = po.plotting.plot_zernike(wf_from_fit - wf, r, theta, zmin=-0.001, zmax=0.001)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### scipy.minimize\n",
    "\n",
    "In the first implementation of wavefront fitting we used `scipy.minimize`: we tried to minimize $l^2$-norm between the given and the constructed wavefront $||W_{given} - W_{constructed}||$. \n",
    "\n",
    "The default minimization method is COBYLA. The decision for that is purely empirical based on the following test (of course we have run it for multiple array sizes, different numbers of zernikes). **This method is much slower than the least-squares method! We recommend using the latter.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "methods = [\n",
    "#     'Nelder-Mead',\n",
    "#     'Powell',\n",
    "#     'CG',\n",
    "#     'BFGS',\n",
    "#     'L-BFGS-B',\n",
    "#     'TNC',\n",
    "    'COBYLA',\n",
    "#     'SLSQP',\n",
    "#     'trust-constr',\n",
    "]\n",
    "po.zernikes.logger.disabled = True\n",
    "for method in methods:\n",
    "    print(method)\n",
    "    %timeit _, _ = po.zernikes.fit_zernikes(wf, js, 'osa', r, theta, cache=True, method=method)\n",
    "print(\"LST SQ\")\n",
    "%timeit _, _ = po.zernikes.fit_zernikes_lstsq(wf, js, 'osa', r, theta, True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### About Caching\n",
    "Fitting a wavefront is an iterative process; we are trying to find coefficients $C_J$ that will give us $W(r, \\theta)=\\sum_{n, m} C_j Z_j(r, \\theta)$. It can be easily noticed that $Z_j(r, \\theta)$, _zernike_mn polynomials, do not change over iterations! As a result they can be stored which gives us a significant performance boost (at the cost of memory consumption). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('With caching: ')\n",
    "%timeit _ = po.zernikes.fit_zernikes(wf, js, 'osa', r, theta, cache=True, method='COBYLA')\n",
    "print('Without caching: ')\n",
    "%timeit _ = po.zernikes.fit_zernikes(wf, js, 'osa', r, theta, cache=False, method='COBYLA')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
