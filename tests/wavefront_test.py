import os
import shutil
import unittest

import astropy.units as u
import numpy as np

from pyoptica.wavefront import Wavefront


class TestWavefront(unittest.TestCase):
    TMP_DIR = 'tmp_data'

    @classmethod
    def setUpClass(cls):
        os.makedirs(cls.TMP_DIR)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.TMP_DIR)

    def test_init(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024
        size = 22528 * u.um
        k = 0.01256637061 / u.nm
        wf_array = np.ones((npix, npix), dtype=np.complex64)

        wf = Wavefront(wavelength, pixel_scale, npix)

        self.assertEqual(wf.wavelength, wavelength)
        self.assertEqual(wf.pixel_scale, pixel_scale)
        self.assertEqual(wf.npix, npix)
        self.assertEqual(wf.size, size)
        # astropy.unit has no 'round' method.
        self.assertAlmostEqual(wf.k.to(1 / u.nm).value, k.to(1 / u.nm).value)
        np.testing.assert_equal(wf.wavefront, wf_array)

    def test_init_coordinates(self):
        min_coord = (-50 * u.um).to(u.m).value
        max_coord = (49.9 * u.um).to(u.m).value

        wavelength = 500 * u.nm
        pixel_scale = 100 * u.nm
        npix = 1000

        wf = Wavefront(wavelength, pixel_scale, npix)
        step = wf.x[0, 1].to(u.m).value - wf.x[0, 0].to(u.m).value

        self.assertAlmostEqual(wf.x[0, 0].to(u.m).value, min_coord)
        self.assertAlmostEqual(wf.y[0, 0].to(u.m).value, min_coord)
        self.assertAlmostEqual(wf.y[-1, -1].to(u.m).value, max_coord)
        self.assertAlmostEqual(wf.x[-1, -1].to(u.m).value, max_coord)
        self.assertAlmostEqual(step, pixel_scale.to(u.m).value)

    def test_repr(self):
        wavelength = 500 * u.nm
        pixel_scale = 100 * u.nm
        npix = 1000

        wf = Wavefront(wavelength, pixel_scale, npix)
        expected = 'Wavefront of wavelength = 500.00 nm, pixel scale = ' \
                   '100.00 nm, size 1000 x 1000.'

        self.assertEqual(repr(wf), expected)

    def test_raises_with_wrong_units_wavelength(self):
        wavelength = 500
        pixel_scale = 22 * u.um
        npix = 1024
        with self.assertRaises(TypeError):
            wf = Wavefront(wavelength, pixel_scale, npix)

    def test_raises_with_wrong_units_pixel_scale(self):
        wavelength = 500 * u.nm
        pixel_scale = 22
        npix = 1024
        with self.assertRaises(TypeError):
            wf = Wavefront(wavelength, pixel_scale, npix)

    def test_property_amplitude(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)
        phase = np.zeros_like(wf.wavefront)
        amplitude = np.ones_like(wf.wavefront) * 12
        wf.amplitude = amplitude

        np.testing.assert_equal(wf.amplitude, amplitude)
        np.testing.assert_equal(wf.phase, phase)

    def test_property_phase(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)
        amplitude = np.ones_like(wf.wavefront)
        phase = np.ones_like(wf.wavefront) * 3.14159
        wf.phase = phase

        np.testing.assert_equal(wf.amplitude, amplitude)
        np.testing.assert_equal(wf.phase, phase)

    def test_property_intensity(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)

        intensity = np.ones_like(wf.wavefront) * 4
        amplitude = np.ones_like(wf.wavefront) * 2
        phase = np.zeros_like(wf.wavefront)
        wf.intensity = intensity

        np.testing.assert_equal(wf.phase, phase)
        np.testing.assert_equal(wf.intensity, intensity)
        np.testing.assert_equal(wf.amplitude, amplitude)

    @staticmethod
    def _test_serialize(wf_created, wf_loaded):
        np.testing.assert_equal(wf_created.wavefront, wf_loaded.wavefront)
        np.testing.assert_equal(wf_created.wavelength, wf_loaded.wavelength)
        np.testing.assert_equal(wf_created.k, wf_loaded.k)
        np.testing.assert_equal(wf_created.x, wf_loaded.x)
        np.testing.assert_equal(wf_created.y, wf_loaded.y)
        np.testing.assert_equal(wf_created.pixel_scale, wf_loaded.pixel_scale)
        np.testing.assert_equal(wf_created.size, wf_loaded.size)
        np.testing.assert_equal(wf_created.npix, wf_loaded.npix)

    def test_serialize_pickle(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)
        outpath = os.path.join(self.TMP_DIR, 'wf.pickle')
        wf.to_pickle(outpath)
        wf_loaded = Wavefront.from_pickle(outpath)
        self._test_serialize(wf, wf_loaded)

    def test_serialize_fits(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)
        outpath = os.path.join(self.TMP_DIR, 'wf.fits')
        wf.to_fits(outpath)
        wf_loaded = Wavefront.from_fits(outpath)
        self._test_serialize(wf, wf_loaded)

    def test_copy(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)
        wf_copy = wf.copy()
        #  The important part is to make sure it is a deep copy -- when copy
        #  is modified, the original is untouched.
        wf_copy.wavefront = np.ones_like(wf.wavefront) * (19 + 88j)
        np.testing.assert_equal(wf.wavefront, np.ones_like(wf.wavefront))

    def test_apply_phase_plotting_threshold(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        wf = Wavefront(wavelength, pixel_scale, npix)
        intensity = np.ones_like(wf.intensity)
        intensity[512, 512] = 0
        wf.intensity = intensity
        phase_for_plotting = wf._apply_phase_plotting_threshold()
        test_set = {
            (phase_for_plotting[512, 512], True),
            (phase_for_plotting[500, 500], False),
        }
        for actual, isnan in test_set:
            with self.subTest(actual=actual, isnan=isnan):
                self.assertEqual(isnan, np.isnan(actual))


if __name__ == '__main__':
    unittest.main()
