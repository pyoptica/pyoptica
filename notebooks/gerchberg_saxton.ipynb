{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"../pyoptica/data/logos/pyoptica_round.gif\" alt=\"PyOptica Logo\" width=\"250\"/>\n",
    "\n",
    "# Gerchberg-Saxton Algorithm\n",
    "\n",
    "The Gerchberg-Saxton algorithm iteratively retrieves phase from two intensity distributions (here called: *Object* and *Target*) by propagating between the two planes. This method is also known as the error-reduction method. The structure of the algorithm can explained when the following flowchart is analyzed: \n",
    "<img src=\"../docs/source/imgs/gs_docstring.png\" alt=\"PyOptica Logo\" width=\"900\"/>\n",
    "\n",
    "The two intensity measurements are *Source intensity* and *Target intensity* which are loaded after the wavefront is propagated to the corresponding plane. THe phase distribution, however, is left unchanged (that's how it is retrieved!). \n",
    "\n",
    "## Literature\n",
    "We really recommend to go through the following positions:\n",
    "1. R. W. Gerchberg and W. O. Saxton (1972), \"A practical algorithm for the determination of the phase from image and diffraction plane pictures,” Optik 35, 237\n",
    "2. Kedar Khare (2016) *Fourier Optics and Computational Imaging*, Wiley&Sons Ltd. (Chapter 17)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's check it out!\n",
    "Let's start with all imports. We are dealing with an iterative algorithm, thus, it would make sense to see how the result changes after each iteration. As a result we will make an animation!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import astropy.units as u\n",
    "from IPython.core.display import HTML\n",
    "import logging\n",
    "from matplotlib import cm\n",
    "import matplotlib.animation as animation\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "import pyoptica as po\n",
    "import pyoptica.holography as poh\n",
    "\n",
    "logging.disable(logging.CRITICAL)  # comment out if you want all the logs!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use PyOptica logo as our target -- we try to find phase distribution (with flat intensity distribution) that after propagating to `z = 20 * u.cm` produces desired intensity distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target = po.utils.get_logo()\n",
    "\n",
    "wavelength = 500 * u.nm\n",
    "pixel_scale = 12 * u.um\n",
    "npix = target.shape[0]\n",
    "wf = po.Wavefront(wavelength, pixel_scale, npix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it is time for the phase distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = 20 * u.cm\n",
    "max_iter = 15\n",
    "randomize_phase = True\n",
    "return_intermediate = True\n",
    "holo, history = poh.retrieve_phase_gs(wf, target, z, max_iter, randomize_phase, return_intermediate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are following tutorial on animations from here: https://matplotlib.org/3.1.1/gallery/animation/dynamic_image.html. Let's look what we start with (the initial wavefront)... There is not much to be seen: uniform intensity and phase."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, (ax_phase, ax_intensity) = plt.subplots(1, 2, figsize=(12, 6))\n",
    "im_phase = ax_phase.imshow(wf.phase, interpolation=None, vmin=-np.pi, vmax=np.pi, cmap=cm.bwr)\n",
    "ax_phase.set_title('Phase distribution in object plane')\n",
    "im_intensity = ax_intensity.imshow((wf * po.FreeSpace(z)).intensity, interpolation=None, vmax=5, cmap='inferno')\n",
    "ax_intensity.set_title('Intensity distribution in target plane')\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def animate(i):\n",
    "    im_intensity.set_array((history[i] * po.FreeSpace(z)).intensity)\n",
    "    im_phase.set_array(history[i].phase)\n",
    "    return [im_intensity, im_phase]\n",
    "\n",
    "anim = animation.FuncAnimation(fig, animate,\n",
    "                               frames=15, interval=250, \n",
    "                               blit=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it's time to check the results after each iteration. Changes in the phase distribution after each iteration are really minor!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HTML(anim.to_jshtml())"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "nteract": {
   "version": "0.15.0"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
