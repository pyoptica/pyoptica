import unittest

import astropy.units as u
import numpy as np

from pyoptica import utils


class TestUtils(unittest.TestCase):

    def test_cart2pol(self):
        rho, phi = utils.cart2pol(1.0, 1.0)
        self.assertAlmostEqual(rho, np.sqrt(2), 5)
        self.assertAlmostEqual(np.rad2deg(phi), 45.0, 5)

    def test_pol2cart(self):
        x, y = utils.pol2cart(np.sqrt(2), np.deg2rad(45.0))
        self.assertAlmostEqual(x, 1, 5)
        self.assertAlmostEqual(y, 1, 5)

    def test_fft(self):
        #  goal is to test if arr is properly shifted!
        #  FFT algoritm is not investigated!!!
        #  After transforming a field of ones, a Dirac delta should be obtained
        #  That means maximum value in the middle
        arr = np.ones((1024, 1024))
        fft_arr = utils.fft(arr)
        max_index = np.argmax(fft_arr, axis=None)
        max_indices = np.unravel_index(max_index, fft_arr.shape)
        expected_max_indices = (512, 512)

        self.assertTupleEqual(max_indices, expected_max_indices)

    def test_ifft(self):
        #  goal is to test if arr is properly shifted!
        #  IFFT algoritm is not investigated!!!
        arr = np.ones((1024, 1024))
        fft_arr = utils.fft(arr)
        max_index = np.argmax(fft_arr, axis=None)
        max_indices = np.unravel_index(max_index, fft_arr.shape)
        expected_max_indices = (512, 512)

        self.assertTupleEqual(max_indices, expected_max_indices)

    def test_fft_ifft(self):
        arr = np.ones((1024, 1024))
        fft_arr = utils.fft(arr)
        fft_ifft_arr = utils.ifft(fft_arr)
        np.testing.assert_array_almost_equal(arr, fft_ifft_arr)

    def test_mesh_grid(self):
        min_coord = (-50 * u.um).to(u.m).value
        max_coord = (49.9 * u.um).to(u.m).value
        pixel_scale = 100 * u.nm
        npix = 1000
        x, y = utils.mesh_grid(npix, pixel_scale)
        step_x = x[0, 1].to(u.m).value - x[0, 0].to(u.m).value
        step_y = y[1, 0].to(u.m).value - y[0, 0].to(u.m).value
        test_set = [
            (x[0, 0].to(u.m).value, min_coord),
            (y[0, 0].to(u.m).value, min_coord),
            (y[-1, -1].to(u.m).value, max_coord),
            (x[-1, -1].to(u.m).value, max_coord),
            (step_x, pixel_scale.to(u.m).value),
            (step_y, pixel_scale.to(u.m).value)
        ]
        for actual, expected in test_set:
            with self.subTest(actual=actual, expected=expected):
                self.assertAlmostEqual(actual, expected)

    def test_space2freq(self):
        npix = 100
        pixel_scale = 1 * u.m
        x, y = utils.mesh_grid(npix, pixel_scale)
        f = utils.space2freq(x)
        g = utils.space2freq(y)
        df = f[0, 1] - f[0, 0]
        dg = g[1, 0] - g[0, 0]
        test_set = [
            (-1 / (2 * pixel_scale), f[0, 0]),
            (-1 / (2 * pixel_scale), g[0, 0]),
            (1 / (2 * pixel_scale) - 1 / (npix * pixel_scale), f[-1, -1]),
            (1 / (2 * pixel_scale) - 1 / (npix * pixel_scale), g[-1, -1]),
            (df, 1 / (pixel_scale * npix)),
            (dg, 1 / (pixel_scale * npix))
        ]
        for actual, expected in test_set:
            with self.subTest(actual=actual, expected=expected):
                actual_m = actual.to(1 / u.m).value
                expected_m = expected.to(1 / u.m).value
                self.assertAlmostEqual(actual_m, expected_m)

    def test_range_validator(self):
        test_range = (0, 100)
        test_set = [
            (0, 0),
            (1, 1),
            (10, 10),
            (100, test_range[1]),
            (101, test_range[1]),
            (1000, test_range[1]),
            (-1, test_range[0]),
            (-10, test_range[0]),
            (-100, test_range[0])
        ]

        for actual, expected in test_set:
            with self.subTest(actual=actual, expected=expected):
                self.assertEqual(utils.range_validator(actual, test_range), expected)

    def test_param_validator(self):
        test_params = ["A", "B", "C"]
        test_set = [
            ("A", "A"),
            ("B", "B"),
            ("C", "C"),
            ("D", "A"),
            ("E", "A"),
            ("F", "A")
        ]

        for actual, expected in test_set:
            with self.subTest(actual=actual, expected=expected):
                self.assertEqual(utils.param_validator(actual, test_params), expected)

    def test_rgb2grayscale(self):
        test_set = [
            (np.ones(shape=(1, 1, 3)), 1),
            (np.ones(shape=(1, 1, 4)), 1),
            (np.array([100, 23, 48]).reshape((1, 1, 3)), 48.873),
            (np.array([100, 23, 48, 210391203]).reshape((1, 1, 4)), 48.873),
        ]
        for rgb, expected in test_set:
            with self.subTest(rgb=rgb, expected=expected):
                actual = utils.rgb2gray(rgb).item()
                self.assertAlmostEqual(expected, actual)

    def test_get_logo_raises(self):
        with self.assertRaises(KeyError):
            utils.get_logo('YOLO')

    def test_get_logo(self):
        test_set = [
            ('rect', 0, 0, 0),
            ('round', 500, 500, 0.3518745306134224),
            ('text', 512, 400, 0.3518745306134224)
        ]
        for kind, x, y, expected in test_set:
            with self.subTest(kind=kind, x=x, y=y, expected=expected):
                actual = utils.get_logo(kind)[x, y]
                self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
