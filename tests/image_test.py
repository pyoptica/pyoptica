import os
import shutil
import unittest

import astropy.units as u
import numpy as np

from pyoptica.image import Image


class TestImage(unittest.TestCase):
    TMP_DIR = 'tmp_data'

    @classmethod
    def setUpClass(cls):
        os.makedirs(cls.TMP_DIR)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.TMP_DIR)

    def test_init(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024
        image_array = np.ones((npix, npix), dtype=np.float)
        image = Image(wavelength, pixel_scale, npix)
        np.testing.assert_array_equal(image.image, image_array)

    def _test_serialize(self, image, image_loaded):
        test_set = [
            (image.wavelength, image_loaded.wavelength),
            (image.k, image_loaded.k),
            (image.x, image_loaded.x),
            (image.y, image_loaded.y),
            (image.pixel_scale, image_loaded.pixel_scale),
            (image.size, image_loaded.size),
            (image.npix, image_loaded.npix)
        ]
        for expected, actual in test_set:
            with self.subTest(expected=expected, actual=actual):
                np.testing.assert_equal(expected, actual)

    def test_serialize_fits(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        image = Image(wavelength, pixel_scale, npix)
        outpath = os.path.join(self.TMP_DIR, 'image.fits')
        image.to_fits(outpath)
        image_loaded = Image.from_fits(outpath)
        self._test_serialize(image, image_loaded)

    def test_serialize_pickle(self):
        wavelength = 500 * u.nm
        pixel_scale = 22 * u.um
        npix = 1024

        image = Image(wavelength, pixel_scale, npix)
        outpath = os.path.join(self.TMP_DIR, 'image.pickle')
        image.to_pickle(outpath)
        image_loaded = Image.from_pickle(outpath)
        self._test_serialize(image, image_loaded)


if __name__ == '__main__':
    unittest.main()
