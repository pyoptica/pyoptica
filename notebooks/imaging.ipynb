{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"../pyoptica/data/logos/pyoptica_round.png\" alt=\"PyOptica Logo\" width=\"100\"/>\n",
    "\n",
    "# Coherent Imaging System\n",
    "\n",
    "In the introductory notebook we discussed imaging properties of a single lens. Here, in this notebook, we broaden the discussion beyond a single lens to more complex imaging systems composed of multiple lenses with different distances between them. Firstly coherent imaging systems are discussed. \n",
    "\n",
    "Following the traditional approach (e.g. [1], [2]) we adopt the generalized model in which properties of all elements can be aggregated into the entrance and exit pupils; propagation o flight between the pupils can be described by geometrical optics.\n",
    "\n",
    "Imaging in a coherent imaging system can be described by:\n",
    "\n",
    "$$U_{i}(x, v)=h(u-\\xi, v-\\eta) \\otimes U_{g}(\\xi, \\eta), $$\n",
    "\n",
    "where $U_{i}(u, v)$ is the field in the image plane, $U_{g}(\\xi, \\eta)$ ideal image predicted by geometrical optics (of object $U_{o}(\\xi, \\eta)$):\n",
    "\n",
    "$$ U_{g}(\\xi, \\eta)=\\frac{1}{\\left|M_{t}\\right|} U_{o}\\left(\\frac{\\xi}{M_{t}}, \\frac{\\eta}{M_{t}}\\right),$$\n",
    "\n",
    "$M_t$ the transverse magnification of the system, $h(u, v)$ is the impulse response system (following Goodman's naming convention the amplitude point-spread function, amplitude PSF). For completeness we should move to reduced variables in the space object:\n",
    "\n",
    "$$\\tilde{\\xi}=M \\xi, \\quad \\tilde{\\eta}=M \\eta.$$\n",
    "\n",
    "In the first paragraph it was stated that an imaging system can be described just using its pupils. What is the relation with the pupil then? Amplitude PSF is:\n",
    "$$h(u-\\tilde{\\xi}, v-\\tilde{\\eta})=\\frac{A}{\\lambda z_{i}} \\int_{-\\infty}^{\\infty} P(x, y) \\exp \\left\\{-\\mathrm{j} \\frac{2 \\pi}{\\lambda z_{i}} \\left( (u-\\tilde{\\xi}) x+(v-\\tilde{\\eta}) y \\right)\\right\\} d x d y$$\n",
    "\n",
    "where $P(x, y)$ is the exit pupil function. Therefore, a diffraction-limited system produces image as a convolution of the image predicted by the geometrical optics with psf that is the Fraunhofer diffraction pattern of the exit pupil. \n",
    "\n",
    "We know the theory know. How can we compute that? We know that convolution can be computed very efficiently using the convolution theorem (similarly to the propagation): \n",
    "\n",
    "$$f \\ast g =\\mathfrak{F}^{-1}\\left\\{\\mathfrak{F}\\{ f \\} \\cdot \\mathfrak{F} \\{g\\} \\right\\}.$$\n",
    "\n",
    "Therefore, the imaging equation becomes:\n",
    "\n",
    "$$G_{i}\\left(f_{U}, f_{V}\\right)=H\\left(f_{U}, f_{V}\\right) G_{g}\\left(f_{U}, f_{V}\\right),$$\n",
    "\n",
    "where, $G_{i}=\\mathfrak{F}\\{U_i\\}$, $G_{g}=\\mathfrak{F}\\{U_g\\}$, $H=\\mathfrak{F}\\{h\\}$ – coherent image transfer function which is strictly connected with the exit pupil: \n",
    "\n",
    "$$H\\left(f, g\\right)=P\\left(-\\lambda z_{X P} f,-\\lambda z_{X P} g\\right)$$\n",
    "\n",
    "$z_{X P}$ distance from the exit pupil to the imaging plane. \n",
    "\n",
    "Lastly we need to discuss couple of remaining parameters defining an imaging system:\n",
    "* [Numerical Aperture (NA)](https://en.wikipedia.org/wiki/Numerical_aperture) – gives us information about the maximal angle accepted by the system. Voelz in [2] uses f-number in computational implementation in the linked wikipedia article you can get description of both including discussion of the difference between the ratio $z_{XP}/w$ where $w$  size of the exit pupil. Please note that we always use the *exit* pupil no the *entrance* one.\n",
    "\n",
    "* NA gives as the cutoff frequency $f_0 = NA/\\lambda$ – the maximal frequency transmitted through the system, so: \n",
    "\n",
    "$$H\\left(f, g\\right) = \\left\\{\\begin{matrix}\n",
    " 1, & f^2 + g^2 \\leq f_0^2 \\\\ \n",
    " 0, & elsewhere\n",
    "\\end{matrix}\\right.$$\n",
    "\n",
    "* $n_o$ – refractive index on the object side;\n",
    "* $n_i$ – refractive index on the image side.\n",
    "\n",
    "More theoretical information can be found in Goodman's Introduction to Fourier Optics (Chapter 6) [1] the software implementation can be found in Voelz's Computational Fourier Optics (Chapter 7) [2].\n",
    "\n",
    "## Literature\n",
    "The theoretical introduction as well as the software were based on: \n",
    "1. Joseph W. Goodman (2004) *Introduction to Fourier Optics*, W. H. Freeman\n",
    "2. David Voelz (2011) *Computational Fourier Optics Matlab Tutorial*, Spie Press"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Software Implementation\n",
    "Let's start with importing all necessary packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline \n",
    "from io import BytesIO\n",
    "\n",
    "from astropy.modeling.functional_models import AiryDisk2D\n",
    "import astropy.units as u\n",
    "from matplotlib import cm\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from PIL import Image\n",
    "import requests\n",
    "\n",
    "import pyoptica as po"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are going to load a test pattern for our imaging system [USAF 1951](https://en.wikipedia.org/wiki/1951_USAF_resolution_test_chart) (the image will be downloaded from wikipedia) and load it as intensity distribution of a wavefront. We need to apply some cosmetic change to obtain a square shape. This pattern provides numerous targets in both orientations that can be used to verify performance of our imaging system.\n",
    "\n",
    "We are going to assume red HeNe laser with wavelength `wavelength = 633.2` nm and `pixel_scale = 100` nm and `npix = 1200`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "img_url = \"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/USAF-1951.svg/1024px-USAF-1951.svg.png\"\n",
    "response = requests.get(img_url)\n",
    "img = Image.open(BytesIO(response.content))\n",
    "\n",
    "img_arr = np.array(img)\n",
    "img_arr = img_arr[:, :, 3]\n",
    "img_arr = img_arr[30:1054]\n",
    "img_arr = img_arr / img_arr.max()  # Just normalization to 1\n",
    "img_arr = np.pad(img_arr, 88, 'constant')\n",
    "\n",
    "wavelength = 633.2 * u.nm\n",
    "pixel_scale = 0.1 * u.um\n",
    "npix = 1200\n",
    "\n",
    "wf = po.Wavefront(wavelength, pixel_scale, npix)\n",
    "wf.intensity = img_arr\n",
    "_ = wf.plot(\n",
    "    intensity='default', \n",
    "    phase='default',\n",
    "    fig_options=dict(dpi=130)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's Image!\n",
    "\n",
    "Let's image the wavefront with an optical system set up for this wavefront with `NA = 0.1`. We should start with checking the PSF. If you are sharped-eyed you most likely have noticed that we imported the `AriyDisk2D`; based on Fourier optics the predicted PSF of an imaging system with circular aperture is an Airy disk! We are going to use it to verify if our computational methods are correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "na = 0.1\n",
    "coh_factor = 0\n",
    "img_system = po.ImagingSystem(wavelength, pixel_scale, npix, coh_factor, na=na)\n",
    "img_system.calculate()\n",
    "_ = img_system.plot(\n",
    "    psf=dict(log_scale=True, vmin=10e-6),\n",
    "    fig_options=dict(figsize=(5, 5), dpi=100),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In astropy r is normalized ([see the docs](https://docs.astropy.org/en/stable/api/astropy.modeling.functional_models.AiryDisk2D.html)). We don't need that, thus, `r` must be multiplied with `r_z`, position of the first zero of J1 (the normalization factor)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psf = np.abs(img_system.psf) ** 2\n",
    "I_normalization = psf.max()\n",
    "x, y = po.utils.mesh_grid(npix, pixel_scale)\n",
    "x1, x2 = x[0, 0], x[-1, -1]\n",
    "y1, y2 = y[0, 0], y[-1, -1]\n",
    "r_z = 1.2196698912665045\n",
    "r =  wavelength / (na * 2) * r_z\n",
    "airy_disk = AiryDisk2D(I_normalization, 0, 0, r)\n",
    "psf_theoretical = airy_disk(x, y)\n",
    "_ = img_system.plot(\n",
    "    psf=dict(\n",
    "        z_function=lambda x: np.abs(x)**2-psf_theoretical,\n",
    "        title='$|PSF_{img\\_sys}|^2 - |PSF_{theoretical}|^2$',\n",
    "        log_scale=True,\n",
    "        vmin=10e-10\n",
    "    ),\n",
    "    fig_options=dict(figsize=(5, 5), dpi=100),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results are almost the same which proves correctness of our method. \n",
    "\n",
    "Now is time to verify if our software is able to efficiently image input wavefronts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image = img_system.image_wavefront(wf)\n",
    "_ = image.plot(image=dict(vmax=1), fig_options=dict(figsize=(5, 5), dpi=100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Something went wrong... The obtained image does not really resemble th initial one. Is it the software or the parameters of the system? Let's investigate the issue! We should start with the cutoff frequency $f_0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We cannot image elements smaller than 6.3 um and even then only the first and zeroth diffraction orders are captured. What if we increase the NA?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "na = 0.96\n",
    "img_system.na = na\n",
    "img_system.calculate()\n",
    "image = img_system.image_wavefront(wf)\n",
    "_ = image.plot(image=dict(vmax=1), fig_options=dict(dpi=100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We managed to produce an image!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
