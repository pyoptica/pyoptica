import unittest
from unittest import mock

import astropy.units as u
import numpy as np
from astropy.modeling.functional_models import AiryDisk2D
from testfixtures import logcapture

from pyoptica import ImagingSystem, Wavefront, utils, CircularAperture


class TestImagingSystem(unittest.TestCase):
    def test_init(self):
        wavelength = 1 * u.um
        pixel_scale = 10 * u.um
        npix = 1024
        coh_factor = 1
        m = 0.75
        na = 0.5
        n_o = 0.8
        n_i = 0.9
        img_sys = ImagingSystem(
            wavelength, pixel_scale, npix, coh_factor, m, na, n_o, n_i
        )
        self.assertEqual(img_sys.wavelength, wavelength)
        self.assertEqual(img_sys.pixel_scale, pixel_scale)
        self.assertEqual(img_sys.npix, npix)
        self.assertEqual(img_sys.coherence_factor, coh_factor)
        self.assertEqual(img_sys.m, m)
        self.assertEqual(img_sys.na, na)
        self.assertEqual(img_sys.n_o, n_o)
        self.assertEqual(img_sys.n_i, n_i)
        self.assertEqual(img_sys.x[0, 0], -5120 * u.um)
        self.assertEqual(img_sys.y[0, 0], -5120 * u.um)
        self.assertEqual(img_sys.x[-1, -1], 5110 * u.um)
        self.assertEqual(img_sys.y[-1, -1], 5110 * u.um)
        self.assertDictEqual(img_sys._zernike_coefs, dict())
        self.assertTrue(img_sys._system_modified, True)

    def test_set_attr(self):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        img_sys._system_modified = False
        img_sys.wavelength = 200 * u.nm
        self.assertTrue(img_sys._system_modified)

    def test_f_g(self):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        f = img_sys.f
        g = img_sys.f
        for i in [f, g]:
            test_set = [
                (i[0, 0], -0.05 * 1 / u.nm), (i[500, 500], 0 / u.nm),
                (i[1, 1] - i[0, 0], 1 / pixel_scale / npix)
            ]
            for actual, expected in test_set:
                with self.subTest(actual=actual, expected=expected):
                    actual_m = actual.to(1 / u.nm).value
                    expected_m = expected.to(1 / u.nm).value
                    self.assertAlmostEqual(actual_m, expected_m)

    def test_load_zernikes_mn(self):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        # j, m, n [(0, 0, 0), (4, 0, 2), (6, -3, 3), (5, 2, 2), (9, 3, 3)]
        z_coefs = {
            (0, 0): 0.1, (0, 2): 0.1, (-3, 3): 0.1, (2, 2): 0.1, (3, 3): 0.1
        }
        img_sys.load_zernikes(z_coefs, 'mn')
        self.assertDictEqual(z_coefs, img_sys._zernike_coefs)

    def test_load_zernikes_osa(self):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        z_coefs = {0: 0.1, 4: 0.1, 6: 0.1, 5: 0.1, 9: 0.1}
        img_sys.load_zernikes(z_coefs, 'OSA')
        expected = {0: 0.1, 4: 0.1, 6: 0.1, 5: 0.1, 9: 0.1}
        self.assertDictEqual(expected, img_sys._zernike_coefs)

    def test_atf_is_limited(self):
        wavelength = 1 * u.um
        pixel_scale = 10 * u.mm
        npix = 1024
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        img_sys._calc_atf()
        np.testing.assert_equal(np.isfinite(img_sys.atf), True)

    def test_diffraction_limited_atf(self):
        wavelength = 13.5 * u.nm
        pixel_scale = 5 * u.nm
        npix = 1024
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        test_set = [(0, 0, 0), (512, 512, 1), (512, 891, 1), (512, 892, 0)]

        diffraction_limited_H = img_sys._calc_diffraction_limited_atf()
        for x, y, h_expected in test_set:
            with self.subTest(x=x, y=y, h_expected=h_expected):
                h_actual = diffraction_limited_H[y, x]
                self.assertEqual(h_actual, h_expected)

    def test_apply_radiometric_correction_m_025(self):
        test_sets = [
            [(0, 0, 0), (512, 512, 1), (512, 491, 0), (512, 492, 2.12261954)],
            [(0, 0, 0), (512, 512, 1), (512, 491, 0), (512, 492, 1)]
        ]
        ms = [0.25, 1]
        for test_set, m in zip(test_sets, ms):
            self._test_radiometric_correction(m, test_set)

    def _test_radiometric_correction(self, m, test_set):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1024
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor, m=m)
        diffraction_limited_H = img_sys._calc_diffraction_limited_atf()
        r_corrected_h = img_sys._apply_radiometric_correction(
            diffraction_limited_H)
        for x, y, h_expected in test_set:
            with self.subTest(x=x, y=y, h_expected=h_expected):
                h_actual = r_corrected_h[y, x]
                self.assertAlmostEqual(h_actual, h_expected, 7)

    def test_apply_aberrations_abs_the_same(self):
        wavelength = 500 * u.nm
        pixel_scale = 100 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        z_coefs = {1: 0.1, 2: 0.2}
        img_sys.load_zernikes(z_coefs, 'osa')
        diffraction_limited_H = img_sys._calc_diffraction_limited_atf()
        H_with_abers = img_sys._apply_aberrations(diffraction_limited_H)
        np.testing.assert_array_almost_equal(
            np.abs(diffraction_limited_H), np.abs(H_with_abers)
        )

    def test_apply_aberrations_correct_wavefront(self):
        wavelength = 500 * u.nm
        pixel_scale = 100 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        z_coefs = {1: 0.1, 2: 0.2}
        img_sys.load_zernikes(z_coefs, 'osa')
        diffraction_limited_H = img_sys._calc_diffraction_limited_atf()
        H_with_abers = img_sys._apply_aberrations(diffraction_limited_H)
        test_set = [
            (650, 594, 0.394 * 2 * np.pi), (461, 344, -0.234 * 2 * np.pi)
        ]
        for x, y, expected in test_set:
            with self.subTest(x=x, y=y, expected=expected):
                actual = np.angle(H_with_abers)[y, x]
                self.assertAlmostEqual(actual, expected)

    def test_psf(self):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1024
        coh_factor = 1
        na = 1
        img_sys = ImagingSystem(
            wavelength, pixel_scale, npix, coh_factor, na=na)
        img_sys.calculate()
        psf = np.abs(img_sys.psf) ** 2
        I_normalization = psf.max()
        x, y = utils.mesh_grid(npix, pixel_scale)
        r_z = 1.2196698912665045
        r = wavelength / (na * 2) * r_z
        airy_disk = AiryDisk2D(I_normalization, 0, 0, r)
        np.testing.assert_array_almost_equal(psf, airy_disk(x, y), 8)

    def test_otf(self):
        wavelength = 200 * u.nm
        pixel_scale = 20 * u.nm
        npix = 1024
        coh_factor = 1
        na = 1
        img_sys = ImagingSystem(
            wavelength, pixel_scale, npix, coh_factor, na=na)
        img_sys.calculate()
        f, g = img_sys.f, img_sys.g
        otf_theoretical = self._theoretical_otf(f, g, na, wavelength)
        np.testing.assert_array_almost_equal(otf_theoretical, img_sys.otf, 3)

    def test_mtf(self):
        wavelength = 200 * u.nm
        pixel_scale = 20 * u.nm
        npix = 1024
        coh_factor = 1
        na = 1
        img_sys = ImagingSystem(
            wavelength, pixel_scale, npix, coh_factor, na=na)
        img_sys.calculate()
        f, g = img_sys.f, img_sys.g
        otf_theoretical = self._theoretical_otf(f, g, na, wavelength)
        mtf_thoretical = np.abs(otf_theoretical)
        np.testing.assert_array_almost_equal(mtf_thoretical, img_sys.mtf, 3)

    def test_ptf(self):
        # nie mam pomyslu. Myslalem o tym:
        # https://www.researchgate.net/publication/225541085_On_the_relation_between_the_wave_aberration_function_and_the_phase_transfer_function_for_an_incoherent_imaging_system_with_circular_pupil
        pass

    def _theoretical_otf(self, f, g, na, wavelength):
        # I am using 7.31 in Computational Fourier Optics Matlab Tutorial
        rho = np.hypot(f, g)
        rho0 = 2 * na / wavelength
        d = (rho / rho0).value
        d[rho > rho0] = 0
        otf_theoretical = 2 / np.pi * (np.arccos(d) - d * np.sqrt(1 - d ** 2))
        otf_theoretical[rho > rho0] = 0
        otf_theoretical = otf_theoretical.astype(np.complex)
        return otf_theoretical

    def test_check_wavefront_compatibility(self):
        wf = [
            dict(wavelength=1000 * u.nm, pixel_scale=100 * u.um, npix=1000),
            dict(wavelength=1000 * u.nm, pixel_scale=100 * u.um, npix=1000),
            dict(wavelength=1000 * u.nm, pixel_scale=100 * u.um, npix=1000)
        ]
        sys = [
            dict(wavelength=100 * u.nm, pixel_scale=100 * u.mm, npix=1000,
                 coherence_factor=1),
            dict(wavelength=1000 * u.nm, pixel_scale=10 * u.mm, npix=1000,
                 coherence_factor=1),
            dict(wavelength=1000 * u.nm, pixel_scale=100 * u.mm, npix=100,
                 coherence_factor=1)
        ]
        for wf_data, sys_data in zip(wf, sys):
            with self.subTest(wf_data=wf_data, sys_data=sys_data):
                self._test_check_wavefront_compatibility_raise(
                    wf_data, sys_data)

    def _test_check_wavefront_compatibility_raise(self, wf_data, img_sys_data):
        wf = Wavefront(**wf_data)
        img_sys = ImagingSystem(**img_sys_data)
        with self.assertRaises(RuntimeError):
            _ = img_sys._check_wavefront_compatibility(wf)

    def test_image_wavefront_magnification(self):
        wavelength = 1000 * u.nm
        pixel_scale = 10 * u.um
        npix = 1024
        coh_factor = 1
        m = 0.25
        wf = Wavefront(wavelength, pixel_scale, npix)
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor, m)
        img_sys.calculate()
        imaged_wf = img_sys.image_wavefront(wf)
        expected = 2.5 * u.um
        self.assertEqual(imaged_wf.pixel_scale, expected)

    def test_image_wavefront_coherence_algo(self):
        test_set = [
            (0, '_image_wavefront_fully_incoherent'),
            (1, '_image_wavefront_fully_coherent'),
            (0.5, '_image_wavefront_partially_coherent'),
        ]
        for coh_factor, method in test_set:
            with mock.patch.object(ImagingSystem, method) as mock_m:
                wl = 1000 * u.nm
                pixel_scale = 10 * u.um
                npix = 1024
                wf = Wavefront(wl, pixel_scale, npix)
                img_sys = ImagingSystem(wl, pixel_scale, npix, coh_factor)
                img_sys.calculate()
                _ = img_sys.image_wavefront(wf)
                self.assertTrue(mock_m.called)

    @logcapture.log_capture()
    def test_warn_if_modified(self, capture):
        wl = 1000 * u.nm
        pixel_scale = 10 * u.um
        npix = 1024
        wf = Wavefront(wl, pixel_scale, npix)
        img_sys = ImagingSystem(wl, pixel_scale, npix, 1)
        _ = img_sys.image_wavefront(wf)
        capture.check(
            (
                'pyoptica.imaging_system.ImagingSystem',
                'WARNING',
                'The system has been modified, however, it has not been '
                'recalculated. The results may be inaccurate. Please rerun '
                '`calculate` to obtain results that represent the current '
                'state of the system.'
            )
        )

    @logcapture.log_capture()
    def test_doesnt_warn_if_not_modified(self, capture):
        wl = 1000 * u.nm
        pixel_scale = 10 * u.um
        npix = 1024
        wf = Wavefront(wl, pixel_scale, npix)
        img_sys = ImagingSystem(wl, pixel_scale, npix, 1)
        img_sys.calculate()
        _ = img_sys.image_wavefront(wf)
        capture.check()

    def test_image_wavefront_raises(self):
        wl = 1000 * u.nm
        pixel_scale = 10 * u.um
        npix = 1024
        coh_factor = 1500
        wf = Wavefront(wl, pixel_scale, npix)
        img_sys = ImagingSystem(wl, pixel_scale, npix, coh_factor)
        with self.assertRaises(ValueError):
            _ = img_sys.image_wavefront(wf)

    def test_image_fully_coherent(self):
        wl = 1000 * u.nm
        pixel_scale = 10 * u.um
        npix = 1024
        wf = Wavefront(wl, pixel_scale, npix)
        wf *= CircularAperture(100 * pixel_scale)
        img_sys = ImagingSystem(wl, pixel_scale, npix, 1)
        image = img_sys._image_wavefront_fully_coherent(wf)
        np.testing.assert_array_almost_equal(wf.intensity, image.image)

    def test_image_fully_incoherent(self):
        wl = 1000 * u.nm
        pixel_scale = 10 * u.um
        npix = 1024
        wf = Wavefront(wl, pixel_scale, npix)
        wf *= CircularAperture(100 * pixel_scale)
        img_sys = ImagingSystem(wl, pixel_scale, npix, 0)
        image = img_sys._image_wavefront_fully_incoherent(wf)
        np.testing.assert_array_almost_equal(wf.intensity, image.image)

    def test_aberrations_plot_special(self):
        wavelength = 500 * u.nm
        pixel_scale = 10 * u.nm
        npix = 1000
        coh_factor = 1
        img_sys = ImagingSystem(wavelength, pixel_scale, npix, coh_factor)
        z_coefs = {0: 0.1, 4: 0.1, 6: 0.1, 5: 0.1, 9: 0.1}
        img_sys.load_zernikes(z_coefs, 'OSA')
        img_sys.calculate()
        wavefront = img_sys._aberrations_plot_special()
        test_set = {
            (wavefront[0, 0], True),
            (wavefront[500, 500], False),
        }
        for actual, isnan in test_set:
            with self.subTest(actual=actual, isnan=isnan):
                self.assertEqual(isnan, np.isnan(actual))
